**Problem Statement:** 
We have some customer records in a text file (customers.txt) -- one customer per line, JSON
lines formatted. We want to invite any customer within 100km of Dublin for some food and
drinks on us. Write a program that will read the full list of customers and output the names and
user ids of matching customers (within 100km), sorted by User ID (ascending).

### To run the program:
node server

Go to browser http://localhost:8080